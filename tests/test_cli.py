# -*- coding: utf-8 -*-

import os
import subprocess
import unittest


class AbstractCliTestCase(unittest.TestCase):
    def execute(self, cmd, env=None):
        process_env = os.environ
        if env is not None:
            process_env.update(env)
        process = subprocess.Popen(cmd, env=process_env, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = process.communicate()
        return_code = process.returncode
        return return_code, stdout, stderr


class ClientConnectTestCase(AbstractCliTestCase):
    def test_default(self):
        return_code, stdout, stderr = self.execute(['ipdb-client-connect', './ipdb.tests.ini', 'userconfig.tmp'], {
            'common_name': 'Default User'
        })
        self.assertEqual(return_code, 0)

    def test_umlaut(self):
        return_code, stdout, stderr = self.execute(['ipdb-client-connect', './ipdb.tests.ini', 'userconfig.tmp'], {
            'common_name': 'Max Müller'
        })
        self.assertEqual(return_code, 0)


class ClientDisconnectTestCase(AbstractCliTestCase):
    pass  # TODO implement
