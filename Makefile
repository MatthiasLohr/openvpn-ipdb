
.PHONY: default dist upload tests clean

default: dist

dist:
	python setup.py sdist bdist_wheel

upload:
	twine upload dist/*

tests:
	python -m unittest discover tests/

clean:
	rm -rf build/ dist/ *.egg-info/
