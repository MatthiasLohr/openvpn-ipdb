# Changelog

## v1.0.0

  * Added support for Umlauts(#1)
  * Added Python 2.7.* version requirement
  * Added Test Cases


## v0.1.0

  * First Test Release
